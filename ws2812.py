"""
Copyright © 2024 Walkline Wang (https://walkline.wang)
Gitee: https://gitee.com/walkline/micropython-ws2812-digital-effect
"""
from machine import Pin
from neopixel import NeoPixel


class WS2812(object):
	def __init__(self, width, height, pin):
		self.__width = width
		self.__height = height
		self.__neopixel = NeoPixel(Pin(pin), self.__width * self.__height)

	def clean(self):
		'''清除屏幕（黑屏）'''
		self.__neopixel.fill((0, 0, 0))

	def fill(self, color:tuple):
		'''填充指定颜色'''
		if isinstance(color, tuple) and len(color) == 3:
			self.__neopixel.fill(color)

	def show(self):
		'''显示所有指定的颜色'''
		self.__neopixel.write()

	def get_pixel(self, index):
		'''获取指定像素的颜色'''
		return self.__neopixel[index]

	def set_pixel(self, index, color:tuple):
		'''设置指定像素的颜色'''
		if isinstance(color, tuple) and len(color) == 3:
			self.__neopixel[index] = color

	@property
	def count(self):
		return self.__neopixel.n
