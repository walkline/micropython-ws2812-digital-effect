<h1 align="center">MicroPython WS2812 Digital Effect</h1>

<p align="center"><img src="https://img.shields.io/badge/Licence-MIT-green.svg?style=for-the-badge" /></p>

## 项目介绍

用 [Matrix Editor](https://gitee.com/walkline/matrix-editor) 设计了几个好玩的数字变化特效，使用`MicroPython`编写了测试代码，借用之前设计的 [抠搜点阵时钟](https://oshwhub.com/Walkline/kou-sou-dian-zhen-shi-zhong) 的屏幕进行显示。

## 效果预览

| effect_1 | effect_2 | effect_3_1 | effect_3_2 |
| :---: | :---: | :---: | :---: |
| ![](./images/digital_effect_1.gif) | ![](./images/digital_effect_2.gif) | ![](./images/digital_effect_3_1.gif) | ![](./images/digital_effect_3_2.gif) |

视频预览可以 [前往B站查看](https://www.bilibili.com/video/BV1bF4m1A7Tm/)

> `effect_2`和`effect_3`是相同风格的变化特效，`effect_3`实现了四个方向的滚动，并且每一帧数据都是实时生成的。

## 硬件介绍

测试用 LED 矩阵规格为 9 x 6 像素，排列顺序示意如下：

```docs
0 | 3 | 6
1 | 4 | 7
2 | 5 | 8
```

## 合作交流

* 联系邮箱：<walkline@163.com>
* QQ 交流群：
	* 走线物联：[163271910](https://jq.qq.com/?_wv=1027&k=xtPoHgwL)
	* 扇贝物联：[31324057](https://jq.qq.com/?_wv=1027&k=yp4FrpWh)

<p align="center"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_walkline.png" width="300px" alt="走线物联"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_bigiot.png" width="300px" alt="扇贝物联"></p>
