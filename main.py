"""
Copyright © 2024 Walkline Wang (https://walkline.wang)
Gitee: https://gitee.com/walkline/micropython-ws2812-digital-effect
"""
from random import randint, choice
from time import sleep_ms
from ws2812 import WS2812
from config import Config
from effects.effect1 import DigitalEffect1
# from effects.effect2 import DigitalEffect2
from effects.effect3 import DigitalEffect3


def random_generator(max):
	while True:
		yield randint(0, max)

def run_test():
	leds    = WS2812(Config.Matrix.WIDTH, Config.Matrix.HEIGHT, Config.Pin.DIN)
	effects = (DigitalEffect1(leds), DigitalEffect3(leds))

	numbers    = random_generator(9)
	directions = random_generator(3)

	leds.clean()
	leds.show()

	last_value_1 = last_value_2 = 0

	while True:
		value = next(numbers)
		effect = choice(effects)
		effect.last_value = last_value_1

		if isinstance(effect, DigitalEffect3):
			effect.show(value, 1, 0, next(directions))
		else:
			effect.show(value, 1, 0)

		last_value_1 = value


		value = next(numbers)
		effect = choice(effects)
		effect.last_value = last_value_2

		if isinstance(effect, DigitalEffect3):
			effect.show(value, 5, 0, next(directions))
		else:
			effect.show(value, 5, 0)

		last_value_2 = value
		sleep_ms(200)


if __name__ == '__main__':
	run_test()
